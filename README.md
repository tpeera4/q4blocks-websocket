Q4BlocksWebService
-----------------------

## Required Dependencies
1. Analyzer tool

```sh
git clone https://bitbucket.org/tpeera4/q4blocks-analysistool.git
```

2. Q4BlocksWebService (this project)

```sh
git clone https://bitbucket.org/tpeera4/q4blocks-webservice.git
```

This is an Eclipse-based project (Dynamic Web Project). 
(Working with Eclipse IDE for Java Developers
Version: Oxygen.2 Release (4.7.2))

# To link the project

1. Properties -> Java Build Path (left panel)
2. Under Projects, Add analyzer project
3. Under Libraries, Add JARS... and select all Java libraries that the analyzer project depends on (all jars in the /libs folder)
4. Back to Properties's left panel -> Deployment Assembly -->Add...
5. Add Project -> Analyzer project
6. Add Java Build Path Entries -> Select all Analyzer projects's dependencies

# To Run

### Setup: Add a server

1. File -> New -> Other.. -> Server
2. click Next
3. Under Apache -> Select Tomcat v8.0 Server
4. click Next
5. Browse the installation directory (create a directory somewhere in your workspace)
(will see the warning text " The Tomcat installation directory is not valid. It is missing expected file or folder...")
6. Click Download and Install... (then wait for it to download, the warning text should disappeared, after everything get downloaded)
7. click Next
8. select the dynamic web project and add it to the right side (to configure it on the server)

### Ready to run

Right click on the dynamic web project -> Run as... -> Run on server -> Select Server (which we have setup) and follow a few steps (default).. finally click finish to run

